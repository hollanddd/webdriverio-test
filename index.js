let Rx = require('rxjs/Rx');
let webdriverio = require('webdriverio');
let options = { desiredCapabilities: { browserName: 'phantomjs' }, port: 4444 };
let browser = webdriverio.remote(options);

let const username = '';
let const pass     = '';

browser.addCommand('login', function async(url, name, pass, cb) {
  return this.url(url)
    .setValue('#email', name)
    .pause(2000)
    .setValue('#pass', pass)
    .pause(2000)
    .submitForm('#login_form')
});

let it = Rx.Observable.fromPromise(
  browser
    .init()
    .login('https://www.facebook.com/login.php', username, pass)
    .waitForVisible('a.fbxWelcomeBoxName', 3000)
    .getText('a.fbxWelcomeBoxName')
)

it.subscribe(
  (name)  => console.log(name),
  (error) => console.log(error),
  ()      => console.log('done')
)
